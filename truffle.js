module.exports = {
    networks: {
        development: {
            host: "127.0.0.1",
            port: 8545,
            network_id: "*"
        }
    },
    solc: {
        enabled: true,
        runs: 200,
    }
};
