import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { Erc20Factory } from './services/erc20-factory';

@Module({
    imports: [],
    providers: [Erc20Factory],
    controllers: [AppController],
})
export class AppModule {}
