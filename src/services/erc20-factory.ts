import { Injectable } from '@nestjs/common';
import * as cp from 'child_process';

@Injectable()
export class Erc20Factory {
    async create(symbol: string): Promise<void> {
        process.env.WEB3_RPC_URL = 'https://kovan.infura.io/v3/fdf54f439d5b4256b5d43167b324135a';
        process.env.WEB3_PRIVATE_KEY = '56c209a6226bbb9a594dd87fd8a418278430ffee674b7f5f4d7b33ce70dd4761';
        process.env.EXPLORER_API_URL = 'https://testnet-explorer.gochain.io/api';

        cp.execSync('web3 contract build /home/chvarkov/Projects/SecurityTokenPlatform/erc20-factory/erc20-factory/ste-erc20.contract.sol');

        cp.execSync('web3 contract deploy /home/chvarkov/Projects/SecurityTokenPlatform/erc20-factory/erc20-factory/SteErc20Contract.bin');
    }
}
