const SteToken = artifacts.require('SteContract');

require('chai').should();

contract('SteContract', accounts => {
    const _name = 'Test ste contract';
    const _symbol = 'TSTX';
    const _decimals = 8;

    before(async () => {
        this.token = await SteToken.new(_name, _symbol, _decimals);
    });

    describe('Token attributes', () => {
       it('Name', async () => {
           const name = await this.token.name();
           name.should.equal(name);
       });

        it('Symbol', async () => {
            const symbol = await this.token.symbol();
            symbol.should.equal(symbol);
        });

        it('Decimals', async () => {
            const decimals = await this.token.decimals();
            decimals.should.equal(decimals);
        });
    })
});
