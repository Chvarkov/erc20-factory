pragma solidity >=0.4.21 <0.7.0;

import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20Detailed.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20Mintable.sol";


contract SteContract is ERC20, ERC20Detailed, ERC20Mintable {
    address public minter;
    address public owner;
    mapping (address => uint) public balances;

    string bank;

    constructor(string memory _name, string memory _symbol, uint8 _decimals, string memory _bank)ERC20Detailed(_name, _symbol, _decimals) public {
        bank = _bank;
    }
}
