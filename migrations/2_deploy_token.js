const SteContact = artifacts.require("./SteContract.sol");

module.exports = function(deployer) {
  const _name = 'Ste contract';
  const _symbol = 'STEC2';
  const _decimals = 8;
  const _bank = 'ATB Bank';
  deployer.deploy(SteContact, _name, _symbol, _decimals, _bank);
};
